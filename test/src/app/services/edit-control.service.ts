import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditControlService {

  private editMode: boolean = false;

  constructor() { }

  isEditMode() {
    return this.editMode;
  }

  setEditMode(value: boolean) {
    this.editMode = value;
  }
}
