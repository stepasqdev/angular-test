import { LoaderService } from './loader.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { API_URL } from '../constants';
import { catchError } from 'rxjs/operators';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Product } from './../models/product';

const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[] = [];
  products$: BehaviorSubject<Product[]> = new BehaviorSubject([]);

  constructor(
    private loaderService: LoaderService,
    private httpClient: HttpClient,
    ) { }

  getProducts() {
    this.loaderService.startLoader();
    this.httpClient.get(API_URL + 'products/all').toPromise().then((res:Product[]) => {
      this.loaderService.endLoader();
      this.products = res; // to mantain a copy
      this.products$.next(res);
    })
  }

  getProduct(productId) {
    return this.httpClient.get(`${API_URL + 'products'}/get/${productId}`).pipe(
      (res) => { 
        return res;
      }
    )
  }

  addProduct(product) {
    this.loaderService.startLoader();
    return this.httpClient.post(`${API_URL}product/add`, product, cudOptions).toPromise().then(res => {
      this.loaderService.endLoader();
      this.getProducts(); // or add manually
      return res;
    });
  }

  updateProduct(product) {
    this.loaderService.startLoader();
    return this.httpClient.post(`${API_URL}product/update`, product, cudOptions).toPromise().then(res => {
      this.loaderService.endLoader();
      this.getProducts(); // or add manually
      return res;
    });
  }

  deleteProduct(product) {
    const id = product.id;
    this.loaderService.startLoader();
    return this.httpClient.delete(`${API_URL}product/delete/${id}`, cudOptions).toPromise().then(res => {
      this.loaderService.endLoader();
      this.getProducts(); // or delete manually
    });
  }

  private handleError(error: any) {
    console.error(error);
    return throwError(error);
  }

}
