import { TestBed } from '@angular/core/testing';

import { APIDetailResolverService } from './apidetail-resolver.service';

describe('APIDetailResolverService', () => {
  let service: APIDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
