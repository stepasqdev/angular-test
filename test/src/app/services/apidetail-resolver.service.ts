import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';

import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class APIDetailResolverService implements Resolve<any> {

  constructor(private productService: ProductService) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.productService.getProduct(route.params.id);
  }

}
