import { TestBed } from '@angular/core/testing';

import { EditControlService } from './edit-control.service';

describe('EditControlService', () => {
  let service: EditControlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditControlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
