import { ConfirmDialogComponent } from './../components/confirm-dialog/confirm-dialog.component';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { EditControlService } from './../services/edit-control.service';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class PageChangeGuard implements CanActivate {

  constructor(
    private router: Router,
    private editControlService: EditControlService,
    public dialog: MatDialog,
    ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(!this.editControlService.isEditMode()) {
      return true;
    } else {
      this.openDialog(state.url);
      return false;
    }
  }

  openDialog(path): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { path: path}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.editControlService.setEditMode(false);
        this.router.navigate([result]);
      }
    });
  }
}