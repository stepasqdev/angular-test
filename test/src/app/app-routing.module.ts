import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductCreationComponent } from './components/product-creation/product-creation.component';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { PageChangeGuard } from './guards/pageChangeGuard';

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full'},
  { path: 'products', component: ProductListComponent , canActivate: [PageChangeGuard]},
  { path: 'products/new', component: ProductCreationComponent , canActivate: [PageChangeGuard]},
  { path: 'product/:id', component: ProductDetailComponent , canActivate: [PageChangeGuard]},

  {path: '404', component: NotFoundComponent },
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
