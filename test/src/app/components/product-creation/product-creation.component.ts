import { EditControlService } from './../../services/edit-control.service';
import { LoaderService } from './../../services/loader.service';
import { ProductService } from './../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-creation',
  templateUrl: './product-creation.component.html',
  styleUrls: ['./product-creation.component.scss']
})
export class ProductCreationComponent implements OnInit {

  form: FormGroup;

  constructor(
    private productService: ProductService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private loaderService: LoaderService,
    private editControlService: EditControlService,
  ) {
    this.form = this.createFormGroup();

    this.form.controls.name.valueChanges.subscribe((val) => {
      if (!this.editControlService.isEditMode()) {
        this.editControlService.setEditMode(true);
      }
    });

  }

  ngOnInit(): void {
  }

  createFormGroup() {
    return new FormGroup({
      name: new FormControl(),
    });
  }

  reset() {
    (<FormGroup>this.form).patchValue({ name: '' });
    this.editControlService.setEditMode(false);
  }

  enableAdd() {
    return this.form.dirty && this.formValue().name != '';
  }

  formValue() {
    return this.form.value;
  }

  goToList() {
    this.router.navigate(['/']);
  }

  onSubmit() {
    const newProduct = this.form.value;
    this.editControlService.setEditMode(false);
    this.loaderService.startLoader();
    this.productService.addProduct(newProduct).then(res => {
      this.loaderService.endLoader();
      this.openSnackBar('Added ' + newProduct.name);
      this.reset();
    });
  }

  openSnackBar(text, action?) {
    this._snackBar.open(text, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
}
