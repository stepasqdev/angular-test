import { EditControlService } from './../../services/edit-control.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  path: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  isEdit: boolean = false;

  ngOnInit(): void {
    this.isEdit = this.editControlService.isEditMode()
  }

  constructor(
    private editControlService: EditControlService,
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
     this.isEdit = this.editControlService.isEditMode()
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
