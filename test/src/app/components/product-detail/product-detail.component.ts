import { EditControlService } from './../../services/edit-control.service';
import { LoaderService } from './../../services/loader.service';
import { Component, OnInit } from '@angular/core';

import { Product } from './../../models/product';
import { ProductService } from './../../services/product.service';

import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  form: FormGroup;

  initialProduct: Product = new Product(-1, 'No Product');

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private editControlService: EditControlService,
    private loaderService: LoaderService,
  ) {
    this.loaderService.startLoader();
    this.form = this.createFormGroup();


    this.productService.getProduct(this.activatedRoute.snapshot.params.id).toPromise().then((product: Product) => {

      this.initialProduct = product;
      (<FormGroup>this.form).patchValue({ name: product.name, id: product.id });
      this.loaderService.endLoader();

      this.form.controls.name.valueChanges.subscribe((val) => {
        if (!this.editControlService.isEditMode()) {
          this.editControlService.setEditMode(true);
        }
      });

    })

   

  }

  createFormGroup() {
    return new FormGroup({
      id: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {

  }

  reset() {
    (<FormGroup>this.form).patchValue({ name: '' });
  }

  enableEdit() {
    return this.form.dirty && this.formValue().name != this.initialProduct.name && this.formValue().name != '';
  }

  delete() {
    this.productService.deleteProduct(this.formValue().id).then(res => {
      this.router.navigate(['/']);
    });
  }

  goToList() {
    this.router.navigate(['/']);
  }

  onSubmit() {
    this.editControlService.setEditMode(false);
    this.productService.updateProduct(this.formValue())
  }

  formValue() {
    return this.form.value;
  }

}
