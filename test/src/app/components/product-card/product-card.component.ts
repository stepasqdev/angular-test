import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product;
  @Output() onRemove = new EventEmitter<any>();

  nameNotFound = "N/A";

  constructor() { }

  ngOnInit(): void {
  }

  delete(event) {
    event.stopPropagation();
    this.onRemove.emit(this.product);
  }

}
