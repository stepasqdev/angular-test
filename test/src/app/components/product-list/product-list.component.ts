import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from './../../models/product';
import { ProductService } from './../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products: Product[] = [];

  constructor(
    private productService: ProductService,
    private router: Router,
    ) { 
    productService.products$.subscribe(val => {
      this.products = val;
    });
  }

  ngOnInit(): void {
    this.productService.getProducts();
  }

  add() {
    this.router.navigate(['/products/new']);
  }

  toDetail(product) {
    this.router.navigate(['/product/', product.id]);
  }

  remove(product) {
    this.productService.deleteProduct(product.id)
  }
}
