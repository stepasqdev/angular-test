import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { API_URL } from '../constants';


@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  products = []

  constructor() {
    this.products = [
      { id: 1, name: 'Product 1' },
      { id: 2, name: 'Product 2' },
      { id: 3, name: 'Product 3' },
      { id: 4, name: 'Product 4' },
      { id: 5, name: 'Product 5' }
    ];
  }

  getIds() {
    return this.products.map(el => el.id)
  }

  getMaxId() {
    const productsIds = this.getIds();
    return Math.max(...productsIds);
  }

  getPositionById(id) {
    return this.getIds().indexOf(id);
  }

  handleRoute(url, method, headers, body, next, request) {

    switch (true) {
      case url.includes(API_URL + 'products/all') && method === 'GET':
        return this.getProducts();
      case url.includes(API_URL + 'products/get') && method === 'GET':
        return this.getProduct(url);
      case url.includes(API_URL + 'product/update') && method === 'POST':
        return this.updateProduct(body);
      case url.includes(API_URL + 'product/add') && method === 'POST':
        return this.addProduct(body);
      case url.includes(API_URL + 'product/delete') && method === 'DELETE':
        return this.deleteProduct(url);
      default:
        // pass through any requests not handled above
        return next.handle(request);
    }
  }

  getProducts() {
    return this.ok(this.products);
  }

  getProduct(url) {
    const id = this.idFromUrl(url);
    const searched = this.products.filter(elem => elem.id == id)
    return this.ok(searched.length > 0 ? searched[0] : []);
  }
  updateProduct(product) {
    if (product && product.id) {
      const position = this.getPositionById(product.id);
      let currentProduct = this.products[position];
      currentProduct.name = product.name;
    }
    return this.ok(this.products);
  }
  addProduct(newProduct) {
    newProduct.id = this.getMaxId() + 1;
    this.products.push(newProduct);
    return this.ok(true);
  }

  deleteProduct(url) {
    const id = this.idFromUrl(url);
    const position = this.getPositionById(id);
    this.products.splice(position,1);
    return this.ok(this.products);
  }

  // // helper functions

  ok(body?) {
    return of(new HttpResponse({ status: 200, body }));
  }

  // error(message) {
  //   return throwError({ error: { message } });
  // }


  idFromUrl(url) {
    const urlParts = url.split('/');
    return parseInt(urlParts[urlParts.length - 1]);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;
    return of(null)
      .pipe(
        mergeMap(() => this.handleRoute(url, method, headers, body, next, request)),
          delay(500),
        ) as Observable<HttpEvent<any>>
  }
}

export const fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true,
};