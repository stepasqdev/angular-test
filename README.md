# Project Test for Angular 11 #

Run `npm i` before running the server

Run `npm start` to run the project

The app is hosted in http://localhost:4200/

### Database

All the default data stored are contained on the file `src\app\interceptors\FakeBackendInterceptor.ts`


### What is inside

- Angular version 11.2.5
- Basic CRUD operations (Create new product, Read one product or all the products, Update a product, Delete a product)
- Remove the `fakeBackendProvider` from the providers of `app.module.ts` and customize the `API_URL` constant to use real API
- Routing with different pages: one for list the products, one for create a product and one for see details and update or delete a product
- A loader is showed when an API was called
- Reactive Form is used
- When a route is changed without saving the data in one Form a dialog appears to choose how to proceed
- Responsive Layout
- Default test cases

- Styled with scss
- Bootstrap
- Angular Materials 




<br />
<br />
<br />
<br />


---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).